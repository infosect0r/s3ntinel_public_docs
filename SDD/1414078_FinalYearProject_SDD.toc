\select@language {english}
\contentsline {section}{\numberline {1}Introduction}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}Design Overview}{2}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Requirement Traceability Matrix}{2}{subsection.1.2}
\contentsline {section}{\numberline {2}System Architecture Design}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}Chosen System Architecture}{2}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Discussion of Alternative Designs}{2}{subsection.2.2}
\contentsline {section}{\numberline {3}System Interface Description}{3}{section.3}
\contentsline {subsection}{\numberline {3.1}Detailed Description of Components}{3}{subsection.3.1}
\contentsline {subsubsection}{\numberline {3.1.1}Component 1: Parsing Rules}{3}{subsubsection.3.1.1}
\contentsline {subsubsection}{\numberline {3.1.2}Component 2: APK Decompilation}{3}{subsubsection.3.1.2}
\contentsline {subsubsection}{\numberline {3.1.3}Component 3: Report Generation}{3}{subsubsection.3.1.3}
\contentsline {subsubsection}{\numberline {3.1.4}Component 4: Android Debug Bridge(adb) Module}{4}{subsubsection.3.1.4}
\contentsline {section}{\numberline {4}User Interface Design}{5}{section.4}
\contentsline {subsection}{\numberline {4.1}Screen Images}{5}{subsection.4.1}
\contentsline {section}{\numberline {5}System Architecture}{6}{section.5}
\contentsline {subsection}{\numberline {5.1}Use Case Specification using template 1}{7}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Use Case Specification using template 2}{8}{subsection.5.2}
\contentsline {section}{\numberline {6}Data Flow Diagrams}{8}{section.6}
\contentsline {subsection}{\numberline {6.1}Level 0 DFD}{8}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Level 1 DFD}{9}{subsection.6.2}
