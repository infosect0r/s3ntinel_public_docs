\contentsline {section}{\numberline {1}Introduction}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}Project Overview}{2}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Project Deliverables}{2}{subsection.1.2}
\contentsline {section}{\numberline {2} Project Organization}{2}{section.2}
\contentsline {subsection}{\numberline {2.1} Software Process Model}{2}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Roles and responsibilities}{2}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3} Tools and techniques}{3}{subsection.2.3}
\contentsline {subsubsection}{\numberline {2.3.1}Hardware Requirements}{3}{subsubsection.2.3.1}
\contentsline {subsubsection}{\numberline {2.3.2}Software Requirements}{3}{subsubsection.2.3.2}
\contentsline {section}{\numberline {3}Project Management Plan}{3}{section.3}
\contentsline {subsection}{\numberline {3.1} Tasks}{3}{subsection.3.1}
\contentsline {subsubsection}{\numberline {3.1.1}Task 1: SRS (Software Requirement Specification) - T1}{3}{subsubsection.3.1.1}
\contentsline {subparagraph}{Description}{3}{section*.2}
\contentsline {subparagraph}{Deliverables and Milestones}{3}{section*.3}
\contentsline {subparagraph}{Resources Needed}{3}{section*.4}
\contentsline {subparagraph}{Dependencies and constraints}{3}{section*.5}
\contentsline {subparagraph}{Risks and contingencies}{3}{section*.6}
\contentsline {subsubsection}{\numberline {3.1.2}Task 2 : Software Design Document - T2}{3}{subsubsection.3.1.2}
\contentsline {subparagraph}{Description}{3}{section*.7}
\contentsline {subparagraph}{Deliverables and Milestones}{3}{section*.8}
\contentsline {subparagraph}{Resources Needed}{3}{section*.9}
\contentsline {subparagraph}{Dependencies and constraints}{3}{section*.10}
\contentsline {subparagraph}{Risks and contingencies}{3}{section*.11}
\contentsline {subsubsection}{\numberline {3.1.3}Task 3 : Code and Test - T3}{3}{subsubsection.3.1.3}
\contentsline {subparagraph}{Description}{3}{section*.12}
\contentsline {subparagraph}{Deliverables and Milestones}{4}{section*.13}
\contentsline {subparagraph}{Resources Needed}{4}{section*.14}
\contentsline {subparagraph}{Dependencies and constraints}{4}{section*.15}
\contentsline {subparagraph}{Risks and contingencies}{4}{section*.16}
\contentsline {subsection}{\numberline {3.2} Timetable}{4}{subsection.3.2}
