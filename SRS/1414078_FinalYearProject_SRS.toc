\contentsline {section}{\numberline {1}Introduction}{2}
\contentsline {subsection}{\numberline {1.1}Document Purpose}{2}
\contentsline {subsection}{\numberline {1.2}Product Scope}{2}
\contentsline {subsection}{\numberline {1.3}Intended Audience and Document Overview}{2}
\contentsline {subsection}{\numberline {1.4}Definitions,Acronyms and Abbreviations}{3}
\contentsline {subsection}{\numberline {1.5}Overall Description}{3}
\contentsline {subsection}{\numberline {1.6}References}{3}
\contentsline {subsection}{\numberline {1.7}Overview of Developer's Responsibilities}{3}
\contentsline {section}{\numberline {2}General Description}{3}
\contentsline {subsection}{\numberline {2.1}Product Perspective}{3}
\contentsline {subsection}{\numberline {2.2}User Characteristics}{4}
\contentsline {section}{\numberline {3}Information Description}{4}
\contentsline {subsection}{\numberline {3.1}Entities and Relationships}{4}
\contentsline {subsection}{\numberline {3.2}Data Dictionary}{4}
\contentsline {subsection}{\numberline {3.3}Data Flow}{4}
\contentsline {section}{\numberline {4}Functional Requirements}{4}
\contentsline {section}{\numberline {5}Specific Requirements}{5}
\contentsline {subsection}{\numberline {5.1}External Interface Requirements}{5}
\contentsline {subsubsection}{\numberline {5.1.1}User Interfaces}{5}
\contentsline {subsubsection}{\numberline {5.1.2}Hardware Interfaces}{5}
\contentsline {subsubsection}{\numberline {5.1.3}Software Interfaces}{5}
\contentsline {subsubsection}{\numberline {5.1.4}Communication Interfaces}{5}
\contentsline {subsection}{\numberline {5.2}Functional Requirements}{5}
\contentsline {subsubsection}{\numberline {5.2.1}APK Decompilation}{5}
\contentsline {subsubsection}{\numberline {5.2.2}URL Identification}{5}
\contentsline {subsubsection}{\numberline {5.2.3}URL Checking}{6}
\contentsline {subsubsection}{\numberline {5.2.4}AndroidManifest Feature Extraction}{6}
\contentsline {subsubsection}{\numberline {5.2.5}File Parsing}{6}
\contentsline {subsection}{\numberline {5.3}Behaviour Requirements}{7}
\contentsline {subsubsection}{\numberline {5.3.1}Use Case View}{7}
\contentsline {section}{\numberline {6}Non-Functional Requirements}{8}
\contentsline {subsection}{\numberline {6.1}Performance Requirements}{8}
\contentsline {subsection}{\numberline {6.2}Safety and Security Requirements}{8}
\contentsline {subsection}{\numberline {6.3}Software Quality Attributes}{8}
\contentsline {subsubsection}{\numberline {6.3.1}Standards Compliance}{8}
\contentsline {subsubsection}{\numberline {6.3.2}Availability}{8}
\contentsline {subsubsection}{\numberline {6.3.3}Timeliness}{8}
\contentsline {subsubsection}{\numberline {6.3.4}Maintainability}{8}
